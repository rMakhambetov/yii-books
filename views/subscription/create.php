<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Subscription */
/* @var $form ActiveForm */

$this->title = 'Subscribe to author';
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['/book/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriptions">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'phone')
            ->widget(MaskedInput::className(),['mask'=>'+7 (999) 999-99-99'])
            ->textInput(['placeholder'=>'+7 (999) 999-99-99'])
            ->label('Ваш Телефон'); ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- subscriptions -->
