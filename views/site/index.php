<?php

use yii\grid\GridView;

/* @var $dataProvider \yii\data\ActiveDataProvider*/
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'year',
            'author.fullname'
        ],
    ]) ?>
</div>
