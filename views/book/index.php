<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->user->isGuest === false):?>
        <p>
            <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'year',
            'isbn',
            'description:ntext',
            'author.fullname',
            [
                'attribute' => 'image',
                'content' => function ($model) {
                    return Html::img("/upload/images/{$model->image}", ['width' => 100, 'height' => 100]);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visible' => Yii::$app->user->isGuest === false
            ],
            [
                'value' => function ($model) {
                    return Html::a('subscribe', "/subscription/create?author_id={$model->author_id}");
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>
</div>
