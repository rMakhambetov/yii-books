<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $year int */
/* @var $years int[]*/

$this->title = 'Top Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('/book/top', 'get') ?>
    <p>
        <?= Html::dropDownList('year', $year, [null => ''] + $years) ?>
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
    </p>

    <?= Html::endForm(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fullname',
            'booksCount',
        ],
    ]); ?>
</div>
