<?php

namespace app\controllers;

use app\models\Subscription;
use Yii;

class SubscriptionController extends \yii\web\Controller
{
    public function actionCreate($author_id)
    {
        $model = new Subscription();
        $model->author_id = $author_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Subscription created');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

}
