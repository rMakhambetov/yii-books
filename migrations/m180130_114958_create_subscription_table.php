<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscription`.
 */
class m180130_114958_create_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscriptions', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->notNull(),
            'author_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscriptions');
    }
}
