<?php

use yii\db\Migration;

/**
 * Class m180122_120423_alter_column_unicode
 */
class m180122_120423_alter_column_unicode extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('books', 'title', $this->string()->append('CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'));
        $this->alterColumn('authors', 'fullname', $this->string()->append('CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'));
        $this->insert('authors', ['fullname' => 'А.С. Пушкин']);
        $this->insert('authors', ['fullname' => 'Ф.М. Достоевский']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180122_120423_alter_column_unicode cannot be reverted.\n";

        return false;
    }
    */
}
