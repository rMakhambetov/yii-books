<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180121_162557_create_table_authors
 */
class m180121_162557_create_table_authors extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('authors', [
            'id' => Schema::TYPE_PK,
            'fullname' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('authors');
    }
}
