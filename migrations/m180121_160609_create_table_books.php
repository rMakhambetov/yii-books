<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180121_160609_create_table_books
 */
class m180121_160609_create_table_books extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => Schema::TYPE_PK,
            'title' => $this->string()->notNull(),
            'year' => $this->integer(),
            'isbn' => $this->string(),
            'description' => $this->text(),
            'image' => $this->string(),
            'author_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('books');
    }
}
