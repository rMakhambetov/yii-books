<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 * @property string $fullname
 *
 * Relations:
 * @property Book[] $books
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname'], 'required'],
            [['fullname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['author_id' => 'id']);
    }

    /**
     * @param int $year
     * @param int $limit
     * @return array
     */
    public static function getTop($year, $limit = 10)
    {
        return self::find()->joinWith(['books' => function ($subquery) use($year) {
                if ($year !== null) {
                    $subquery->onCondition(['books.year' => $year]);
                }
            }])
            ->select(['authors.*', 'COUNT(books.id) AS booksCount'])
            ->groupBy(['authors.id'])
            ->orderBy(['booksCount' => SORT_DESC])
            ->limit($limit)
            ->createCommand()
            ->queryAll();
    }
}
