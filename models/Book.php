<?php

namespace app\models;

use app\commands\SMSJob;
use mongosoft\file\UploadBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $title
 * @property int $year
 * @property string $isbn
 * @property string $description
 * @property string $image
 * @property integer $author_id
 *
 * Relations:
 * @property Author $author
 */
class Book extends ActiveRecord
{

    const EVENT_NEW_BOOK = 'new-book';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->queue->push(new SMSJob(['author_id' => $this->author_id]));
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'author_id'], 'required'],
            ['year', 'integer'],
            ['description', 'safe'],
            [['title', 'isbn'], 'string', 'max' => 255],
            ['author_id', 'exist', 'targetClass' => Author::class, 'targetAttribute' => 'id'],
            ['image', 'file', 'extensions' => 'jpg, jpeg, png, gif', 'on' => ['insert', 'update']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'year' => 'Year',
            'isbn' => 'Isbn',
            'description' => 'Description',
            'image' => 'Image',
            'author_id' => 'author'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image',
                'scenarios' => ['insert', 'update'],
                'path' => '@webroot/upload/images',
                'url' => '@web/upload/images',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }
}
