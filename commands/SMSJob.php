<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Subscription;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yiidreamteam\smspilot\SmsPilot;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SMSJob extends BaseObject implements JobInterface
{
    public $author_id;

    public function execute($queue)
    {
        $api = new SmsPilot();
        $subscriptions = Subscription::find()->where(['author_id' => $this->author_id])->all();
        foreach ($subscriptions as $subscription) {
            try {
                $api->send($subscription->phone, 'Author has new book!');
            } catch (\Exception $e) {}
        }
    }
}